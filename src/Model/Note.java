package Model;

import java.time.LocalDateTime;

public class Note {
    private String title;
    private LocalDateTime timeStamp;
    private String noteText;

    public Note(String title,  LocalDateTime timeStamp, String text)
    {
        this.title = title;
        this.timeStamp = timeStamp;
        this.noteText = text;
    }

    public String getTitle() { return title; }
    public LocalDateTime getTimeStamp() { return timeStamp; }
    public String getNoteText(){ return noteText; }

    public void setTitle(String newTitle){
        title = newTitle;
    }
    public void setNoteText(String newNoteText){
        noteText = newNoteText;
    }
}
