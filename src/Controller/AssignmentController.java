package Controller;

import Model.Assignment;
import Model.Module;
import Model.Profile;
import Model.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class AssignmentController {

    private Stage previousStage;

    @FXML private Label assignmentLabel;
    @FXML private Text assignmentDescription;
    @FXML private Button editMilestone;
    @FXML private Button setRequirementsButton;
    @FXML private Button viewGanttButton;
    @FXML private Button buttonReading;
    @FXML private Button buttonWriting;
    @FXML private Button buttonProgramming;
    @FXML private Button buttonWatching;
    @FXML private Button buttonSurveying;
    @FXML private Button buttonCreating;
    @FXML private Button buttonResearching;
    @FXML private ProgressBar milestoneProgressBar;

    @FXML private Button goToPreviousScene;

    private Profile profile;
    private Module selectedModule;
    private Assignment selectedAssignment;
    UIManager ui = new UIManager();

    Button[] arrayButtons = new Button[7];

    public void setPreviousStage(Stage stage){ this.previousStage = stage; }

    public void initialize(){
        assignmentLabel.setText(selectedAssignment.getName());
        assignmentDescription.setText(selectedAssignment.getDescription());

        viewGanttButton.setOnAction(value ->  {
            try {
                System.out.println("Still needs coding");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        setRequirementsButton.setOnAction(value ->  {
            try {
                ui.loadRequirement(profile, selectedAssignment);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        editMilestone.setOnAction(value ->  {
            try {
                ui.loadMilestone(profile, selectedAssignment);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        goToPreviousScene.setOnAction(value ->  {
            try {
                ui.openPreviousScene();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        arrayButtons[0] = buttonReading;
        arrayButtons[1] = buttonWriting;
        arrayButtons[2] = buttonProgramming;
        arrayButtons[3] = buttonWatching;
        arrayButtons[4] = buttonSurveying;
        arrayButtons[5] = buttonCreating;
        arrayButtons[6] = buttonResearching;

        buttonReading.setDisable(true);
        buttonWriting.setDisable(true);
        buttonProgramming.setDisable(true);
        buttonWatching.setDisable(true);
        buttonSurveying.setDisable(true);
        buttonCreating.setDisable(true);
        buttonResearching.setDisable(true);

        for(Task t : selectedAssignment.getTasks()){
            for(Button btn : arrayButtons){
                String buttonText = btn.getText();
                if(t.getTaskType().toString().equalsIgnoreCase(buttonText)){
                    btn.setDisable(false);
                }
            }
        }

        if(selectedAssignment.getMilestone() != null) {
            milestoneProgressBar.setProgress(selectedAssignment.getMilestone().calculateMilestoneProgress());
        }
        //milestoneProgressBar.setProgress(0.5);

    }

    public AssignmentController(){}
    public AssignmentController(Task t){ enableButton(t); }

    public Module getSelectedModule() {
        return selectedModule;
    }

    public Assignment getSelectedAssignment() {
        return selectedAssignment;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setUi(UIManager ui) {
        this.ui = ui;
    }

    public void setProfile(Profile currentProfile){
        this.profile = currentProfile;
    }

    public void enableButton(Task t){
        for(Button btn : arrayButtons){
            if(t.getTaskType().name().equalsIgnoreCase(btn.getText())){
                btn.setDisable(false);
            }
        }
    }

    public void onTaskClick(ActionEvent event) throws IOException {
        if(selectedAssignment.getMilestone()!=null){
            milestoneProgressBar.setProgress(selectedAssignment.getMilestone().calculateMilestoneProgress());
        }
        UIManager ui = new UIManager();
        Button button = (Button) event.getSource();
        String buttonText = button.getText();
        switch (buttonText) {
            case "Reading":
                System.out.println("Reading");
                ui.loadTask(selectedAssignment.getTasks().get(0), profile, selectedAssignment);
                break;
            case "Writing":
                System.out.println("Writing");
                ui.loadTask(selectedAssignment.getTasks().get(1), profile, selectedAssignment);
                break;
            case "Programming":
                System.out.println("Programming");
                ui.loadTask(selectedAssignment.getTasks().get(2), profile, selectedAssignment);
                break;
            case "Watching":
                System.out.println("Watching");
                ui.loadTask(selectedAssignment.getTasks().get(3), profile, selectedAssignment);
                break;
            case "Surveying":
                System.out.println("Surveying");
                ui.loadTask(selectedAssignment.getTasks().get(4), profile, selectedAssignment);
                break;
            case "Creating":
                System.out.println("Creating");
                ui.loadTask(selectedAssignment.getTasks().get(5), profile, selectedAssignment);
                break;
            case "Researching":
                System.out.println("Researching");
                ui.loadTask(selectedAssignment.getTasks().get(6), profile, selectedAssignment);
                break;
            default:
                System.out.println("Invalid choice");
                break;
        }

    }

    public void setSelectedAssignment(Assignment assignment){
        this.selectedAssignment = assignment;
    }

}
