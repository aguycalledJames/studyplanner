package Controller;

import Model.Activity;
import Model.Assignment;
import Model.Profile;
import Model.Task;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.util.Calendar;

public class CreateActivityController {

    // Lists:
    @FXML private ListView<Task> tasks;
    @FXML private TextField NameActivity;
    @FXML private TextField initialValueTextField;
    @FXML private TextField completedValueTextField;
    @FXML private TextArea NotesActivity;

    @FXML private Button submitButton;
    @FXML private Button cancelButton;
    @FXML private CheckBox completeCheckBox;


    ObservableList<Activity> activityList;

    private Task selectedTask;
    private Assignment selectedAssignment;
    private Module selectedModule;
    private Activity existingActivity;
    private UIManager ui = new UIManager();
    private Profile profile;

    private void onSubmitClick() throws Exception {
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if(existingActivity == null){
            Activity activity = new Activity();

            try{
                activity.setActivityName(NameActivity.getText());
                activity.setActivityNotes(NotesActivity.getText());
                activity.setInitialValue(Double.parseDouble(initialValueTextField.getText()));
                if(completedValueTextField.getText().isEmpty()){
                    activity.setCompletedValue(0);
                    activity.setComplete(completeCheckBox.isSelected());
                }
                else if(Double.parseDouble(completedValueTextField.getText()) ==
                        Double.parseDouble(initialValueTextField.getText())){
                    activity.setComplete(true);
                }
                else{
                    activity.setCompletedValue(Double.parseDouble(completedValueTextField.getText()));
                    activity.setComplete(completeCheckBox.isSelected());
                }
                activity.setDayOfWeek(dayOfWeek);
                activity.addTask(selectedTask);
            }
            catch (NumberFormatException e){
                System.out.println("Please enter a valid number");
            }
            catch(Exception e){
                System.out.println("A different error occurred");
            }
            selectedTask.addActivity(activity);
            Stage stage = (Stage) this.submitButton.getScene().getWindow();
            stage.close();
            ui.loadTask(selectedTask, profile, selectedAssignment);
        }

        else{
            try{
                selectedTask.removeActivity(existingActivity);
                existingActivity.setActivityName(NameActivity.getText());
                existingActivity.setActivityNotes(NotesActivity.getText());
                existingActivity.setInitialValue(Double.parseDouble(initialValueTextField.getText()));
                existingActivity.setCompletedValue(Double.parseDouble(completedValueTextField.getText()));
                existingActivity.setComplete(completeCheckBox.isSelected());
                existingActivity.addTask(selectedTask);
                existingActivity.setDayOfWeek(dayOfWeek);
            }
            catch (NumberFormatException e){
                System.out.println("Please enter a valid number");
            }
            catch(Exception e){
                System.out.println("A different error occurred");
            }
            selectedTask.addActivity(existingActivity);
            Stage stage = (Stage) this.submitButton.getScene().getWindow();
            stage.close();
            ui.loadTask(selectedTask, profile, selectedAssignment);
        }
    }

    public void setActivityList(ObservableList<Activity> activityList) {
        this.activityList = activityList;
    }

    public void setExistingActivity(Activity activity){
        this.existingActivity = activity;
    }

    public void initialize()
    {
        submitButton.setOnAction(value ->  {
            try {
                onSubmitClick();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

/*        for(Task.TaskType taskType : Task.TaskType.values()){
            taskChoiceBox.getItems().addAll(taskType);
        }

        this.addChangeListeners(assignmentChoiceBox);

        assignmentChoiceBox.setValue(selectedAssignment);
        if(selectedAssignment != null && !selectedAssignment.getEndDate().
                before(new Date())) assignmentChoiceBox.setValue(selectedAssignment);*/

        System.out.println(selectedAssignment.getName());

        if(existingActivity != null){
            NameActivity.setText(existingActivity.getActivityName());
            initialValueTextField.setText(String.valueOf(existingActivity.getInitialValue()));
            completedValueTextField.setText(String.valueOf(existingActivity.getCompletedValue()));
            completeCheckBox.setSelected(existingActivity.isComplete());
            NotesActivity.setText(existingActivity.getActivityNotes());
        }
        //assignmentChoiceBox.getItems().addAll(se);
        //moduleChoiceBox.getItems().addAll(profile.getModules());
    }

    public void setAssignment(Assignment assignment){
        this.selectedAssignment = assignment;
    }

    public void setProfile(Profile currentProfile){
        this.profile = currentProfile;
    }

    public void setTask(Task task){
        this.selectedTask = task;
    }

    public void addChangeListeners(ChoiceBox<Assignment> assignments){
        assignments.valueProperty().addListener(new ChangeListener<Assignment>(){
            @Override
            public void changed(ObservableValue ov, Assignment prev, Assignment cur){
                if(cur!=null){
                }
                else
                {
                    System.out.println("NULLLLL");
                    //taskListView.getItems().clear();
                    //addedTaskListView.getItems().clear();
                }
            }
        });
    }

}
