package Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import java.io.FileWriter;
import java.io.IOException;

public class CreateAccountController  {
    UIManager ui = new UIManager();

    @FXML public Button createAccountBtn;
    @FXML public Button settingsBtn;
    @FXML public TextField inputPWord;
    @FXML public TextField inpStudentID;
    @FXML public TextField inputEmail;
    @FXML public Button logInPage;

    public void CreateAccount(ActionEvent actionEvent) throws Exception {

        /* get the details and parse to a csv file on a new line */
        // first create file object for file placed at location
        // specified by filepath

        if(inputEmail.getText().isEmpty() || inputPWord.getText().isEmpty()){
            throw new Exception("Invalid text");
        }
        try{
            FileWriter pw = new FileWriter("./data.csv", true);
            Hasher Hash = new Hasher(){};
            Hash.setHashedValue(inputPWord.getText());
            pw.append("\n");
            pw.append(inputEmail.getCharacters());
            pw.append(",");
            pw.append(inpStudentID.getCharacters());
            pw.append(",");
            pw.append(Hash.getHashedValue());
            pw.append(",");

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
    public void openSettings(javafx.event.ActionEvent actionEvent) {
    }
    public void openLoginPage(javafx.event.ActionEvent actionEvent){

    }
}
