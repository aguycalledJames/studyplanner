package Controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LoginController {
    public Stage primaryStage;
    UIManager ui = new UIManager();

    @FXML public Button logInBtn;
    @FXML public Button createAccountBtn;
    @FXML public Button settingsBtn;
    @FXML public TextField inputEmail;
    @FXML public TextField inputPWord;
    public Label logInStatus;

    // gives user the current status of their log in

    public void setPrimaryStage(Stage stage) {
        this.primaryStage = stage;
    }

    public boolean verifyPWord(TextField uName, String hashedPWord){
        String line;
        String cvsSplitBy = ",";
        try (BufferedReader br = new BufferedReader(new FileReader("./data.csv"))) {
            while ((line = br.readLine()) != null) {
                String[] details = line.split(",");
                if (details[0].equals(uName.getText())) {
                    // if email is found, we select the hashed value for the password associated with the email
                    if (hashedPWord.equals(details[2])){
                        return true;
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean searchForEmail(TextField inpEmail) throws IOException {
        String line;
        String cvsSplitBy = ",";
        try (BufferedReader br = new BufferedReader(new FileReader("./data.csv"))) {
            // create a buffered reader on the user info file
            while ((line = br.readLine()) != null) {
                // while there is a next line
                String[] details = line.split(",");
                // split this line at every comma
                if (details[0].equals(inpEmail.getText())) {
                    return true;
                }
                // if the first piece of info in the line is the username. verification is complete
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //if the user name is never found, the verification is false
        return false;
    }

    public void logIn(javafx.event.ActionEvent actionEvent) throws IOException {
        Hasher hasher = new Hasher(){};
        if (!searchForEmail(inputEmail)){
            logInStatus.setText("Couldn't find that email");
        }
        hasher.setHashedValue(inputPWord.getText());
        if (verifyPWord(inputEmail, hasher.getHashedValue()) == false ){
            logInStatus.setText("Password incorrect");
        }else{
            System.out.println("Success");
            logInStatus.setText("Access authorised");
        }
    }

    public void createAccount(javafx.event.ActionEvent actionEvent) throws IOException {
        ui.loadNewAccount();
    }
    public void openSettings(ActionEvent actionEvent) {
    }
}
