package Controller;

import Model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class RequirementController {
    @FXML private TextField moduleTextField;
    @FXML private TextField requirementName;
    @FXML private TextArea requirementDescription;
    @FXML private ListView<Activity> availableActivitiesListView;
    @FXML private ListView<Activity> addedActivitiesListView;
    @FXML private DatePicker dueDatePicker;
    @FXML private Button updateButton;
    @FXML private Button cancelButton;
    @FXML private Button addActivityButton;
    @FXML private Button removeActivityButton;

    private Profile profile;
    private Assignment currentAssignment;
    private ObservableList<Activity> activityObservableList;
    private ArrayList<Activity> requirementArrayList;
    private Activity tempActivity;
    private static Requirement currentRequirement;
    Stage stage;

    public void initialize()
    {
        updateButton.setOnAction(value ->  {
            try {
                onSubmitClick();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        cancelButton.setOnAction(value ->  {
            try {
                handleQuit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        addActivityButton.setOnAction(value ->  {
            try {
                addActivityToAddedList();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        removeActivityButton.setOnAction(value ->  {
            try {
                removeActivityfromAddedList();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        availableActivitiesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        addedActivitiesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        availableActivitiesListView.setCellFactory(param -> new ListCell<Activity>() {
            @Override
            protected void updateItem(Activity activity, boolean empty) {
                super.updateItem(activity, empty);

                if (empty || activity == null || activity.getActivityName() == null) {
                    setText(null);
                } else {
                    setText(activity.getActivityName());
                }
            }
        });
        addedActivitiesListView.setCellFactory(param -> new ListCell<Activity>() {
            @Override
            protected void updateItem(Activity activity, boolean empty) {
                super.updateItem(activity, empty);

                if (empty || activity == null || activity.getActivityName() == null) {
                    setText(null);
                } else {
                    setText(activity.getActivityName());
                }
            }
        });

        for(Task task : currentAssignment.getTasks()){
            for(Activity activity : task.getActivities()){
                availableActivitiesListView.getItems().addAll(activity);
            }
        }

        if(currentRequirement != null){
            activityObservableList = FXCollections.observableList(currentRequirement.getActivities());
            requirementName.setText(currentRequirement.getRequirementName());
            requirementDescription.setText(currentRequirement.getRequirementNotes());
            addedActivitiesListView.setItems(activityObservableList);
        }
        //availableActivitiesListView.getItems().setAll();
    }

    public void setRequirementArrayList(ArrayList<Activity> requirementArrayList) {
        this.requirementArrayList = requirementArrayList;
    }

    public void setRequirementName(TextField name) {
        this.requirementName = name;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public void setCurrentAssignment(Assignment currentAssignment) {
        this.currentAssignment = currentAssignment;
    }

    public ArrayList<Activity> getRequirementArrayList() {
        return requirementArrayList;
    }

    private void onSubmitClick() throws Exception {
            try{
                ObservableList<Activity> addedActivitiesList = addedActivitiesListView.getItems();
                ArrayList<Activity> addedActivitiesArrayList;
                if (addedActivitiesList instanceof ArrayList<?>) {
                    addedActivitiesArrayList = (ArrayList<Activity>) addedActivitiesList;
                } else {
                    addedActivitiesArrayList = new ArrayList<>(addedActivitiesList);
                }

                if(currentRequirement == null){
                    Requirement newRequirement = new Requirement();
                    //System.out.println(milestoneName.getText());
                    //System.out.println(milestoneNotes.getText());
                    newRequirement.setRequirementName(requirementName.getText());
                    newRequirement.setRequirementNotes(requirementDescription.getText());
                    newRequirement.setActivities(addedActivitiesArrayList);
                    newRequirement.setContainingAssignment(currentAssignment);
                    currentRequirement = newRequirement;
                }
                else{
                    currentRequirement.setRequirementName(requirementName.getText());
                    currentRequirement.setRequirementNotes(requirementDescription.getText());
                    currentRequirement.setActivities(addedActivitiesArrayList);
                    currentRequirement.setContainingAssignment(currentAssignment);
                }

                //requirementArrayList.addAll(addedActivitiesArrayList);
                currentAssignment.setRequirements(currentRequirement);

                System.out.println("Successfully added Milestone");
                System.out.println(currentAssignment.getRequirements().getActivities());
                stage.close();

            }
            catch(Exception e){
                System.out.println("A different error occurred");
            }
    }

    private void addActivityToAddedList(){
        stage = (Stage) availableActivitiesListView.getScene().getWindow();
        tempActivity = availableActivitiesListView.getSelectionModel().getSelectedItem();
        addedActivitiesListView.getItems().addAll(tempActivity);
    }

    private void removeActivityfromAddedList(){
        stage = (Stage) availableActivitiesListView.getScene().getWindow();
        tempActivity = addedActivitiesListView.getSelectionModel().getSelectedItem();
        addedActivitiesListView.getItems().remove(tempActivity);
    }

    private void handleQuit(){

    }
}
