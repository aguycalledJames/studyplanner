package Controller;

import Model.*;
import Model.Module;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.util.Date;
import java.util.HashSet;

public class DashboardController {

    @FXML public Label label1;
    @FXML private TableView<Assignment> approachingTable;
    @FXML private TableView<Assignment> passedTable;
    @FXML private TableColumn<Assignment, String> approachingAssignment;
    @FXML private TableColumn<Assignment, Date> approachingDeadline;
    @FXML private TableColumn<Assignment, String> passedAssignment;
    @FXML private TableColumn<Assignment, Date> passedDeadline;

    @FXML private Assignment selectedAssignment;
    @FXML private Task selectedTask;
    @FXML private Milestone selectedMilestone;
    @FXML public GridPane moduleGridPane;
    @FXML private MenuItem saveButton;
    @FXML private MenuItem openButton;
    @FXML private AnchorPane rootPane;

    private static boolean hasBeenInitialized = false;

    UIManager ui = new UIManager();
    MainController mainController = new MainController();
    ProfileController profileController = new ProfileController();
    Profile profile;
    public Stage primaryStage;

    public void setPrimaryStage(Stage stage) {
        this.primaryStage = stage;
    }

    public void initialize() throws Exception {
        mainController.setProfile(profile);
        profileController.setProfile(profile);
        UIManager ui = new UIManager();
        mainController.loadDashboard(moduleGridPane, profile);
        initData();
        handleClick();
        saveButton.setOnAction(value ->  {
            try {
                profileController.saveProfile(profile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    private void handleClick(){
        approachingTable.setOnMouseClicked((MouseEvent event) -> {
            if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2){
                selectedAssignment = approachingTable.getSelectionModel().getSelectedItem();
                try {
                    ui.setTempStage(primaryStage);
                    ui.loadAssignment(selectedAssignment, profile);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        passedTable.setOnMouseClicked((MouseEvent event) -> {
            if (event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2){
                System.out.println(passedTable.getSelectionModel().getSelectedItem());
            }
        });
    }

    public void setProfile(Profile currentProfile){
        this.profile = currentProfile;
    }

    public void onButtonClick() throws Exception {
        //label1.setText("COLD");
        //ui.addActivity();

    }

    public void openStatistics(){
        try {
            ui.loadStatistics(profile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initData(){

        boolean allDone = false;

        PropertyValueFactory<Assignment,String> aName = new PropertyValueFactory<>("name");
        PropertyValueFactory<Assignment,Date> aDate = new PropertyValueFactory<>("endDate");

        approachingAssignment.setCellValueFactory(aName);
        approachingDeadline.setCellValueFactory(aDate);

        passedAssignment.setCellValueFactory(aName);
        passedDeadline.setCellValueFactory(aDate);

        for(Module m : profile.getModules()){
            for(Assignment a : m.getAssignments()){
                Date current = new Date();

                for(Task t : a.getTasks()){
                    if(!t.isTaskComplete()) allDone = false;
                }

                if(current.getTime() < a.getEndDate().getTime())
                    approachingTable.getItems().add(a);
                else
                    passedTable.getItems().add(a);
            }
        }

    }

}
