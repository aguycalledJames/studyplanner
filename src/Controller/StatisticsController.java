package Controller;

import java.time.*;
import java.time.DayOfWeek;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

import Model.*;
import Model.Module;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;

/**
 *
 * @author mattcorcoran
 */
public class StatisticsController implements Initializable {

    @FXML private BarChart<?, ?> DailyHoursChart;
    @FXML private CategoryAxis x;
    @FXML private NumberAxis y;
    @FXML private Label TipText;
    @FXML private PieChart pieChart;
    @FXML private Label workTodayNum;

    private Profile profile;
    //temp vars for days of week
    int monHours = 12;
    int tusHours = 10;
    int wedHours = 2;
    int thursHours = 2;
    int FriHours = 2;
    int SatHours = 2;
    int SunHours = 12;
    //temp vars for task hours
    int readingHours = 5;
    int writingHours = 3;
    int programmingHours = 20;
    int watchingHours = 3;
    int survayingHours = 0;
    int creatingHours = 1;
    int reserchingHours = 9;
    int otherHours = 2;
    // temp username
    String username = "Matt";

    DayOfWeek[] daysOfWeek = DayOfWeek.values();
    Calendar calendar = Calendar.getInstance();
    int currentDayofWeekInt = calendar.get(Calendar.DAY_OF_WEEK);
    DayOfWeek currentdayOfWeek = daysOfWeek[currentDayofWeekInt];


    public void setProfile(Profile profile) { this.profile = profile; }

    public void initData(){
        getTaskHours();
        getDayHours();
    }



    public int getTaskHours() {
        int hourCount = 0;
        int activityHoursDone = 0;
        for (Module m : profile.getModules()) {
            for (Assignment assign : m.getAssignments()) {
                for (Task task : assign.getTasks()) {
                    for (Activity activity : task.getActivities()) {
                        activityHoursDone = (int) activity.getCompletedValue();
                        hourCount = (int) (hourCount + activityHoursDone);
                    }
                }
            }
        }
        return hourCount;
    }

    public int getDayHours() {
        int hourCount = 0;
        int activityHoursDone = 0;
        for (Module m : profile.getModules()) {
            for (Assignment assign : m.getAssignments()) {
                for (Task task : assign.getTasks()) {
                    for (Activity activity : task.getActivities()) {
                        currentDayofWeekInt = activity.getDayOfWeek();
                        currentdayOfWeek = daysOfWeek[currentDayofWeekInt];
                        setWorkTodayNum();
                        activityHoursDone = (int) activity.getCompletedValue();
                        hourCount = (int) (hourCount + activityHoursDone);
                    }
                }
            }
        }
        return hourCount;
    }

    public void setWorkTodayNum(){
        switch(currentdayOfWeek.getValue()) {
            case (1):
                workTodayNum.setText(""+getMonHours());
                break;
            case (2):
                workTodayNum.setText(""+getTusHours());
                break;
            case (3):
                workTodayNum.setText(""+getWedHours());
                break;
            case (4):
                workTodayNum.setText(""+getThusHours());
                break;
            case (5):
                workTodayNum.setText(""+getFriHours());
                break;
            case (6):
                workTodayNum.setText(""+getSatHours());
                break;
            case (7):
                workTodayNum.setText(""+getSunHours());
                break;
        }
    }


    public int getMonHours() {
        return monHours;
    }

    public int getTusHours() {
        return tusHours;
    }

    public int getWedHours() {
        return wedHours;
    }

    public int getThusHours() {
        return thursHours;
    }

    public int getFriHours() {
        return FriHours;
    }

    public int getSatHours() {
        return SatHours;
    }

    public int getSunHours() {
        return SunHours;
    }

    public void setMonHours(int monHours) {
        this.monHours = monHours;
    }
    public void setTusHours(int tusHours) {
        this.tusHours = tusHours;
    }
    public void setWedHours(int wedHours) {
        this.wedHours = wedHours;
    }
    public void setThursHours(int thursHours) {
        this.thursHours = thursHours;
    }

    public void setFriHours(int friHours) {
        FriHours = friHours;
    }

    public void setSatHours(int satHours) {
        SatHours = satHours;
    }

    public void setSunHours(int sunHours) {
        SunHours = sunHours;
    }

    //getters for pie chart
    public int getReadingHours(){
        return readingHours;
    }

    public int getWritingHours(){
        return writingHours;
    }

    public int getProgrammingHours(){
        return programmingHours;
    }

    public int getWatchingHours(){
        return watchingHours;
    }

    public int getSurvayingHours(){
        return survayingHours;
    }

    public int getCreatingHours(){
        return creatingHours;
    }

    public int getReserchingHours(){
        return reserchingHours;
    }

    public int getOtherHours(){
        return otherHours;
    }

    public void setReadingHours(int readingHours) {
        this.readingHours = readingHours;
    }

    public void setWritingHours(int writingHours) {
        this.writingHours = writingHours;
    }

    public void setProgrammingHours(int programmingHours) {
        this.programmingHours = programmingHours;
    }

    public void setCreatingHours(int creatingHours) {
        this.creatingHours = creatingHours;
    }

    public void setReserchingHours(int reserchingHours) {
        this.reserchingHours = reserchingHours;
    }

    public void setSurvayingHours(int survayingHours) {
        this.survayingHours = survayingHours;
    }

    public void setOtherHours(int otherHours) {
        this.otherHours = otherHours;
    }

    public void setWatchingHours(int watchingHours) {
        this.watchingHours = watchingHours;
    }

    //get username
    public String getUsername(){
        return username;
    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        initData();
        setWorkTodayNum();

        int dayVal = currentdayOfWeek.getValue();
        XYChart.Series set1 = new XYChart.Series<>();
        set1.setName(username +" - Weekly Hours Chart");
        set1.getData().add(new XYChart.Data("Monday", getMonHours()));
        if (dayVal > 1){
            set1.getData().add(new XYChart.Data("Tuesday", getTusHours()));

        }
        else {
            set1.getData().add(new XYChart.Data("Tuesday", 0));
        }
        if (dayVal > 2){
            set1.getData().add(new XYChart.Data("Wednesday", getWedHours()));
        }
        else {
            set1.getData().add(new XYChart.Data("Wednesday", 0));
        }
        if (dayVal > 3){
            set1.getData().add(new XYChart.Data("Thursday", getThusHours()));
        }
        else {
            set1.getData().add(new XYChart.Data("Thursday", 0));
        }
        if (dayVal > 4){
            set1.getData().add(new XYChart.Data("Friday", getFriHours()));
        }
        else {
            set1.getData().add(new XYChart.Data("Friday", 0));
        }
        if (dayVal > 5){
            set1.getData().add(new XYChart.Data("Saturday", getSatHours()));
        }
        else {
            set1.getData().add(new XYChart.Data("Saturday", 0));
        }
        if (dayVal > 6){
            set1.getData().add(new XYChart.Data("Sunday", getSunHours()));
        }
        else {
            set1.getData().add(new XYChart.Data("Sunday", 0));
        }

        DailyHoursChart.getData().addAll(set1);

        //TODO - alogorithm for comments
        TipText.setWrapText(true);

        double weeklyAverage = (double)(getMonHours() + getTusHours() +
                getWedHours()+ getThusHours()+ getFriHours() + getSatHours()+
                getSunHours())/7;
        System.out.println(weeklyAverage);
        double todayAverageCal=0;

        //TODO
        switch(currentdayOfWeek.name()) {
            case ("MONDAY"):
                todayAverageCal = (getMonHours()/weeklyAverage);
                break;
            case ("TUESDAY"):
                todayAverageCal = (getTusHours()/weeklyAverage);
                break;
            case ("WEDNESDAY"):
                todayAverageCal = (getWedHours()/weeklyAverage);
                break;
            case ("THURSDAY"):
                todayAverageCal = (getThusHours()/weeklyAverage);
                break;
            case ("FRIDAY"):
                todayAverageCal = (getFriHours()/weeklyAverage);
                break;
            case ("SATURDAY"):
                todayAverageCal = (getSatHours()/weeklyAverage);
                break;
            case ("SUNDAY"):
                todayAverageCal = (getSunHours()/weeklyAverage);
                break;
        }

        NumberFormat formatter = new DecimalFormat("#0.00");

        String Tip1 = "Keep working hard! You are only "+formatter.format(((todayAverageCal*100)))
                + "% off your weekly average! (Calculated over the last 7 days) "
                + "Remember not to overwork yourself"
                + "and take breaks when you need them.";
        String Tip2 = "Great work today! You have beaten your weekly average"
                + "(Calculated over the last 7 days) "
                + "daily work hours by " +formatter.format(((todayAverageCal*100)-100))+ "%! "
                + "Remember to take a break when you feel"
                + " like you need one.";

        if (todayAverageCal >= 1){
            TipText.setText(Tip2);
        }
        if (todayAverageCal < 1){
            TipText.setText(Tip1);
        }

        //pie chart
        ObservableList<PieChart.Data> pieChartData =
                FXCollections.observableArrayList(
                        new PieChart.Data("Reading", getReadingHours()),
                        new PieChart.Data("Writing", getWritingHours()),
                        new PieChart.Data("Programming", getProgrammingHours()),
                        new PieChart.Data("Watching", getWatchingHours()),
                        new PieChart.Data("Survaying", getSurvayingHours()),
                        new PieChart.Data("Creating", getCreatingHours()),
                        new PieChart.Data("Reaserching", getReserchingHours()),
                        new PieChart.Data("Other", getOtherHours())
                );

        pieChart.setData(pieChartData);


    }
}